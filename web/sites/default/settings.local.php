<?php

/**
 * @file
 * Default local settings.
 *
 * Duplicate this file and rename to 'settings.local.php'.
 */

/**
 * READ THIS IF YOU ARE USING LANDO.
 *
 * Uncomment this line and you are good to go.
 */

include 'settings.lando.php';
